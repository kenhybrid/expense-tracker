import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import seo from "./seo";
import "@/assets/sass/main.scss";
const app = createApp(App);
// app.config.globalProperties.$filters = {
//   currencyUSD(value) {
//     return "$ " + value;
//   },
// };
app.use(store).use(seo).use(router).mount("#app");
