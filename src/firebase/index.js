// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore, enableIndexedDbPersistence } from "firebase/firestore";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCUzMGFcB8vb-5xz6BRgH94hsYWMG9Z2dc",
  authDomain: "expense-tracker-daf14.firebaseapp.com",
  projectId: "expense-tracker-daf14",
  storageBucket: "expense-tracker-daf14.appspot.com",
  messagingSenderId: "204505019997",
  appId: "1:204505019997:web:7b6d140df01649d2e5ffb4",
  measurementId: "G-XREZQYQH3W",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
enableIndexedDbPersistence(db);
export { db };
