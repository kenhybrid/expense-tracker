import { createStore, createLogger } from "vuex";
import createPersistedState from "vuex-persistedstate";
import { db } from "../firebase";
import router from "../router";

import {
  collection,
  getDocs,
  addDoc,
  query,
  orderBy,
  doc,
  deleteDoc,
  where,
} from "@firebase/firestore";
export default createStore({
  plugins: [
    createLogger(),
    createPersistedState({
      key: "vuex-admin",
    }),
  ],
  state: {
    transactions: [],
    loading: false,
  },
  getters: {
    getTransactions(state) {
      return state.transactions;
    },
    getCount(state) {
      return state.transactions.length;
    },
    getLoading(state) {
      return state.loading;
    },
    getExpenses(state) {
      const items = [];
      state.transactions.forEach((t) => {
        if (t.type === "expense") {
          items.push(t.amount);
        }
      });
      return items.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue;
      }, 0);
    },
    getIncomes(state) {
      const items = [];
      state.transactions.forEach((t) => {
        if (t.type === "income") {
          items.push(t.amount);
        }
      });
      return items.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue;
      }, 0);
    },
    getSavings(state) {
      const items = [];
      state.transactions.forEach((t) => {
        if (t.type === "income") {
          items.push(t.amount);
        }
      });
      const saving = items.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue;
      }, 0);
      return saving * 0.2;
    },
  },
  mutations: {
    setTransactions(state, payload) {
      state.transactions = payload;
    },
    setLoading(state, payload) {
      state.loading = payload;
    },
    addTransactions(state, payload) {
      state.transactions.unshift(payload);
    },
    deleteTransaction(state, payload) {
      state.transactions.splice(payload.index, 1);
    },
  },
  actions: {
    async getTransactions({ commit }) {
      commit("setLoading", true);
      try {
        const ref = collection(db, "transactions");
        const q = query(ref, orderBy("date", "desc"));
        const documentSnapshots = await getDocs(q);
        const transactions = [];
        documentSnapshots.forEach((doc) => {
          const data = {
            id: doc.id,
            title: doc.data().title,
            date: doc.data().date,
            amount: doc.data().amount,
            type: doc.data().type,
            account: doc.data().account,
          };
          transactions.push(data);
        });
        commit("setTransactions", transactions);
        commit("setLoading", false);
      } catch (error) {
        console.log("Error getting documents: ", error);
        commit("setLoading", false);
      }
    },
    async addTransactions({ commit }, payload) {
      commit("setLoading", true);

      try {
        const ref = collection(db, "transactions");
        const docRef = await addDoc(ref, {
          title: payload.title,
          type: payload.type,
          account: payload.account,
          amount: payload.amount,
          date: payload.date,
        });

        const transsaction = {
          id: docRef.id,
          title: payload.title,
          amount: payload.amount,
          account: payload.account,
          date: payload.date,
          type: payload.type,
        };
        commit("addTransactions", transsaction);
        commit("setLoading", false);
        router.push("/history");

        // setTimeout(() => {
        //   router.push("/history");
        // }, 300);
      } catch (error) {
        console.log("Error getting documents: ", error);
        commit("setLoading", false);
      }
    },
    async deleteTransaction({ commit }, payload) {
      commit("setLoading", true);

      try {
        await deleteDoc(doc(db, "transactions", payload.id));
        const object = {
          index: payload.index,
        };
        commit("deleteTransaction", object);
        commit("setLoading", false);
      } catch (error) {
        console.log("Error getting documents: ", error);
        commit("setLoading", false);
      }
    },
    async sortTransaction({ commit }, payload) {
      commit("setLoading", true);

      try {
        const ref = collection(db, "transactions");
        const q = query(
          ref,
          orderBy("date", "desc"),
          where("date", ">=", payload.date)
        );
        const documentSnapshots = await getDocs(q);
        const transactions = [];
        documentSnapshots.forEach((doc) => {
          const data = {
            id: doc.id,
            title: doc.data().title,
            date: doc.data().date,
            amount: doc.data().amount,
            type: doc.data().type,
            account: doc.data().account,
          };
          transactions.push(data);
        });
        commit("setTransactions", transactions);
        commit("setLoading", false);
      } catch (error) {
        console.log("Error getting documents: ", error);
        commit("setLoading", false);
      }
    },
  },
});
